Lecture 3 - Flask
=================

Flask is the most popular micro-framework for web in Python language.

It has a big community and a lot of third-party plugins. It is mostly used in micro services infrastructures, but it also can be used in monotone application for web.

# Install

You can install flask using `pip install flask` command.

Details: [Installation Docs](https://flask.palletsprojects.com/en/1.1.x/installation/)

# Structure

Major frameworks like Python/Django, PHP/Symfony, Java/Spring have some ideal project structures for the application.

For Django, it is advised to have different apps for each purpose. If you are building a Grocery store management application, you might have apps like "grocery", "products", "customers" and etc.

Each app contains a models.py file where you manage data models for that application. For example, in "products" application you might have models like Product, Inventory, Discounts and etc.

Flask however doesn't have any predefined structure, it is up to developer to make their own structures.

For simple and small application you might have a single "app.py" file where you manage all data models, views, settings and etc.

However, if the application is a bit complex, it might make sense to divide it into different parts and manage them separately.

# Basic application

Lets start with a simple application:

```
from flask import Flask

app = Flask(__name__)

@app.route('/')
def homepage():
    return "Hello World!"

if __name__ == '__main__':
    app.run()
```

You can save it as `app.py` and run this application using `python app.py` or `env FLASK_APP=app.py flask run` and it will be available at `http://127.0.0.1:5000/`.

`FLASK_APP` variable here is an environment variable, that can be added to your current shell environment using `export FLASK_APP=app.py`.

# Routing

Pages in Flask can be added using 2 main ways, `route` decorator and Blueprints.

The main argument in the route command is path of the page. You can also include parameters in this path: `@app.route('/user/<int:user_id>/')` which will produce pages like `/user/42/`.

Default type for arguments is `str` (string). Adding `int:` to the argument's name will convert it to integer and will only match pages with integer value in them.

Available converters:

* `string` - (default).
* `int` - only positive integers.
* `float` - positive floating point values.
* `path` - also a string, but will accept slashes (`/`) too.
* `uuid` - accepts UUID strings.

The slash at the end of path is to show that it is a page and flask will redirect requests that are accessing without using slash at the end.
However, if there is no slash at the end, it will be treated like a file and adding slash in request will give 404 error.

Route decorator also accepts `methods` keyword argument. By default, your views can only accept `GET` method. You can add `methods=["GET", "POST"]` to enable `POST` method too. Same applies for other methods too.

Another method is using Blueprints which can be very useful for complex applications where you can create a modular applications, [Blueprints Docs](https://flask.palletsprojects.com/en/1.1.x/blueprints/).

# URL Building

To get a url for certain pages you can use flask's `url_for()` function (`from flask import url_for`).
This function accepts view name (in this case, function's name) and its arguments, then returns url address for that view: `url_for('homepage')`.


# Working with request

Use `from flask import request` to use request in your views. `request` is a request details that was sent by the client (user) to the flask app. It is a `RequestContext` object in flask.

Available parameters:

* `method` - string name of the HTTP method used in that request. Can be `"GET", "POST", "PATCH"` and etc.
* `path` - shows url path used in that page.
* `args` - arguments passed to that view (`page/?key=value`).
* `files` - contains files that were POSTed to your view (`enctype="multipart/form-data"` forms in HTML). It is a dictionary, where keys are file names.
* `cookies` - Cookies from the user. You can only read these cookies, to set a new cookies, you will need to create a response using `flask.make_response` and then use `set_cookie('key', 'value')` function in that response.

To redirect the user to another view, use `flask.redirect` function. It accepts direct path, so you can use `url_for` function inside it to generate that path.

# Class-Based Views

For a object-oriented views you can write your own class view by extending `flask.views.View` class. Your class should have a `dispatch_request(self, *args, **kwargs)` function that will handle the view context.

Class-based views can have arguments like `methods` and `decorators` where you can specify available view methods and decorators to use in that view.

To add your view to a route, you can use `app.add_url_rule('page_path/', view_func=ViewName.as_view('view_name'))`. It will register your view as its name can be used for `url_for` function.

Another option is to extend `flask.views.MethodView` class where you should write a method for each HTTP method (`def get(self):` and etc.)

[Docs on Class-Based and Functional Views](https://flask.palletsprojects.com/en/1.1.x/api/#class-based-views)

By default, flask expects you to return a page context (can be a rendered template or dictionary for JSON response), status code of the page and headers in a tuple format `(response, status, header)`.

If status is not provided, then default success with context status (200) will be returned. Header also can be added by middlewares, we will talk a bit later about that.

BTW. You can use `flask.json.jsonify` function to convert your python response (like lists) to json.

# Static Files

Flask by default requires you to store static files under the `static/` folder.

To access a "static/css/main.css" file in flask, you can use `url_for('static', filename='css/main.css')` and it will give you path for that file (something like `http://127.0.0.1:5000/static/css/main.css`).

# Templates

For templates, flask by default requires them to be places under `templates/` folder. It can be either in your project root or inside your application.

To return a rendered template from a view you should use `flask.render_template` function.

You can pass your own arguments to that function and they can be used in the template.

Flask uses Jinja templates, it is like HTML, but you can also write your dynamic parameters inside the html (`Hello {{ name }}`) and have some logic (`{% if name %}Hello {{ name }}{% else %}Hello unnamed user{% endif %}`).

[Rendering Docs](https://flask.palletsprojects.com/en/1.1.x/quickstart/#rendering-templates)

# Error handlers

To handle error pages by yourself, you can use `@app.errorhandler(status_code)` views: `@app.errorhandler(404)`.


# Settings - Configuration

You application's configurations are stored in `app.config` object.

You can set configuration directly using `app.config['DEBUG'] = True`.

To load configuration from a file, you can use `app.config.from_pyfile('config_name.cfg`) which will load values from `config_name.cfg` file. You can use `from_json` for a json file.

Another way is to load a python class: `app.config.from_object(ConfigObject)`.

You can also load configuration from the environment using `app.config.from_envvar('ENVIRONMENT_CONFIG_NAME')` but it can be done better using a `dotenv` plugin.

You can get an environment parameter in python using `os.environ.get('PARAM_NAME', default="default_value")`.

Example Config object in Python:

```
class Config(object):
    DEBUG = False
    DATABASE_URI = 'sqlite:///sqlite.db'
```

[Configuration Docs](https://flask.palletsprojects.com/en/1.1.x/config/)

# Sessions

You can track users using a session based on Cookies. Use `flask.session` to access session parameters.

[Sessions Docs](https://flask.palletsprojects.com/en/1.1.x/quickstart/#sessions)

# Database

Flask does not have any built-in libraries to manage database.

You can use simple SQLite 3 in flask (built-in python module) which is a file-based database for simple applications and for local testing.

Check [Using SQLite 3 with Flask](https://flask.palletsprojects.com/en/1.1.x/patterns/sqlite3/#using-sqlite-3-with-flask) documentation for detailed information and a simple example.

# Middleware

Middleware is mostly used to manipulate you application between client (user) and your view. An example usage can be found [here](https://ohadp.com/adding-a-simple-middleware-to-your-flask-application-in-1-minutes-89782de379a1).

But flask recommends using view decorators instead.

# Python Anywhere

For testing purpose, we will host our web applications in [Python Anywhere](https://www.pythonanywhere.com/) which provides a free hosting for flask applications.

# Good to check pages

* [Flask tagged packages in Pypi](https://pypi.org/search/?c=Framework+%3A%3A+Flask)
* [Flask homepage](https://flask.palletsprojects.com/en/1.1.x/)
* [Flask Signals](https://flask.palletsprojects.com/en/1.1.x/api/#signals)
* [Deployment for Flask](https://flask.palletsprojects.com/en/1.1.x/deploying/#deployment) - Gunicorn tutorial is recommended.
* [View Decorators in Flask](https://flask.palletsprojects.com/en/1.1.x/patterns/viewdecorators/#view-decorators)

An example flask application will be posted in this repository for testing.

