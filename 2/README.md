Lecture 2 - Python Language
===========================

Python is an interpreted, high-level, general-purpose programming language.

It can be used on multiple platforms with multiple purposes.
It is easy to write, read and understand, its philosophy is to write beautiful, explicit, simple, readable code. You can read more about it in "The Zen of Python" (https://www.python.org/dev/peps/pep-0020/) or by using "import this" in python.

In this lecture we will try to learn some basics of python language to write our own web application (using the flask framework) and to implement some security testing tools.

# Python 2 vs 3

Support for python 2 will be stopped after January, 2020 (https://pythonclock.org/).
You can easily write programs that will work in Python 2 using Python 3. Most notable difference between these 2 versions is packages. Not all libraries for Python 2 are updated for Python 3,
so sometimes you have to use Python 2 if you need such a library. But even in such cases, using Python 3's syntax works just fine.

# Python IDLE

Python's IDLE is a tool that helps to interactively learn the Python language. Python programs are just a text files written in the Python syntax.
As python is an interpreted language, you can also use its interpreter to directly execute the code line-by-line (or by blocks in some cases).

You can install Python and IDLE in mostly all operating systems, download it from python.org website.

* Windows: download the installer from python.org
* MacOS: MacOS comes with python already installed, but in case if it is missing you can install it using `brew install python3`
* Ubuntu (Debian) Linux: Also should be preinstalled, if not, `apt-get install python3`
* Arch Linux: `pacman -S python3`

To install third-party libraries (packages) in Python you will need `pip` package manager (https://pip.pypa.io/en/stable/installing/)

Basic syntax
============

Everything in Python is an Object. They mostly always have methods and attributes. Even data types (int, float, str, list, set, dict and etc.) are objects and can be extended.
More about extending objects will be covered later.

You do not need to declare a type in python, although Python 3 now has type annotations. Variables and functions in python are mutable, they can be changed.
For example, you can declare a variable as integer and later in the code, if you will assign a string to it, it will become a string type.

You can use `type(variable)` and `isinstance(variable, type)` functions to check the type of the variable.

# Data Types

* `str`: Everything between quotes is treated as a string object. `'`, `"` and `"""` can be used.
* `int`: Integers. Also can be used for Binary, Octal and Hexadecimal numbers.
* `float`: Floating-point number, 3.141592653589793 (`math.pi`)
* `complex`: Complex numbers like 2+3j
* `bool`: Boolean that can be True and False.
* `list`: Arrays.
* `dict`: Dictionary, key-value type.
* `tuple`: Tuple objects.
* `set`: Unique sets.
* `function`: Functions.
* `None`: A none type (or NULL in some languages).
* Custom types using built-in or custom objects (like datetime).

# Basic Functions [5]

* `print`: You can use `print()` function to print anything to the console.
* `len`: Can be used to get length of a list, set or string like objects.
* `bool`: Most types can also be used to convert other variables to that type. Writing `bool(variable)` will convert it to Boolean type. Same applies for `str`, `int`, `float`, `list` and etc.
* `fotmat`: can be used to format strings, like `"Welcome {username} to my app".format(username="admin")`.
* `getattr`: get an attribute from the object.
* `hasattr`: check if an object has certain attribute.
* `help`: print documentation for an object, function or a library. `import math` and then `help(math)`.
* `input`: get an input from user.
* `max` and `min`: get maximum and minimum values.
* `range`: Generate range list from start to an end with custom steps between.
* `sorted`: sort the list or sortable types.
* You can also check `map`, `zip` functions that can be useful in some cases.
*

# Creating a function

You can create a function using `def` keyword (definition):

```
def multiply(a, b):
    return a * b
```

And call them like built-in functions: `multiply(3, 14)`.

You can also have default value for arguments in the function's definition:

```
def multiply(a, b=5):
    return a * b
```

This time, if you will not provide a value for `b` in the function call, it will be assigned to 5: `multiply(2)`.

Note that arguments with a default value should be written after "required" arguments. Try calling `multiply` function without providing a value for `a`.

You can also change the order in the function call by using argument's name: `multiply(b=3, a=5)`.

If there is no a return statement in the function, it will return `None` by default.

# Creating objects

Objects can be created by using `class` keyword. They are also called "Classes" in python.

Classes have some methods to initialize them and print them as strings:

```
class Student(object):
    STATIC_VALUE = "not recommended static value"

    def __init__(self, name, age, profession="Student"):
        self.name = name
        self.age = age
        self.profession = profession

    def __str__(self):
        return self.name

    def __repr__(self):
        return "<Student {name}>".format(name=self.name)
```

Variables created by this class are called Class Instances: `student = Student(name="Emin", age=42, profession="Teacher")`.

`__str__` method is used when calling `str(student)` or `print(student)`. As it is a class method (function), you can also call `student.__str__()`.

`__repr__` is called when you type that instance in the console (`student`). It is instance's representation.

# Methods, Static Methods and Class methods

Classes can have 4 types of methods:

1. Usual functions without any decorator, these are methods that can be used on class instances. The first argument should be `self` - object itself.
2. Property functions - these are like class properties that are mostly added to self inside the `__init__` function. They work like class arguments but can generate data using values defined in classes. Can accept only 1 argument, `self`.
3. Static Methods - these are methods that do not depend on instance or class itself. They do not have any default arguments.
4. Class Methods - these are methods that only depend on class itself and can be used without any instance. Their first argument is `cls` which is Class itself.

Please, read [this](https://realpython.com/instance-class-and-static-methods-demystified/) and [this](https://www.python-course.eu/python3_properties.php) documentations for detailed explanation and examples for these methods.

# Function decorations

Function decorations are very useful in functional programming and it is all about decorating a function :D

First of all, you create a function that accepts another function as its argument.

Inside that function, you declare an inner function that will accept all arguments related to the target function.
You can also use `*args` and `**kwargs` to make your decorator usable for different functions with different arguments.

As a result, your decorator function should return its inner function as reference (without making a call to it).

Now you can implement your own logic inside your decorator function and also inside its inner function before making a call to your actual function.

You can add decorator (you can decorate) to other functions in your program just by adding `@function_name` before the definition of that function (`def`).

Read about decorators and examples [here](https://www.thecodeship.com/patterns/guide-to-python-function-decorators/)

# Imports

Imports are how you import a library in your python program for using something from it.

Other languages have similar method, C/C++ use `#include` to import libraries.

There are some built-in libraries in python that you can import without installing them. One of them is `math` library.

Import the `math` library just by writing `import math` at the beginning of your program.

Now you can use methods and variables from the `math` library: `math.pi`, `math.e`, `math.sin(30)` and etc.

To check all possible methods and variables in the `math` library, you can use `dir(math)` function.
Also, you can read its documentation by calling `help(math)`.

If you only need few methods from the library, you can use structure like:

* `from math import pi` - import only `pi`. Now you can use `pi` in your program.
* `from math import e as e_value` - this will import `e` and assign it to `e_value` variable. You can use `e_value` in your program which will give the result for `e`.

# Recommended reads:

1. Dive Into Python 3: https://diveinto.org/python3/table-of-contents.html
2. Free interactive python language course: https://www.codecademy.com/learn/learn-python-3 (25 hours)
3. Free python hosting: https://www.pythonanywhere.com/
4. Django Implementation for OWASP Top 10: https://github.com/Contrast-Security-OSS/DjanGoat
5. https://docs.python.org/3/library/functions.html
